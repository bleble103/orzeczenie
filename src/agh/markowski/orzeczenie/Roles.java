package agh.markowski.orzeczenie;

public enum Roles {
    PRESIDING_JUDGE,
    REPORTING_JUDGE,
    REASONS_FOR_JUDGMENT_AUTHOR;

    @Override
    public String toString(){
        switch(this){
            case PRESIDING_JUDGE:
                return "Przewodniczący składu sędziowskiego";
            case REPORTING_JUDGE:
                return "Sędzia sprawozdawca";
            case REASONS_FOR_JUDGMENT_AUTHOR:
            default:
                return "Autor uzasadnienia";
        }
    }

    public static Roles toEnum(String role){
        if(role.equals("PRESIDING_JUDGE"))
            return PRESIDING_JUDGE;
        if(role.equals("REPORTING_JUDGE"))
            return REPORTING_JUDGE;
        return REASONS_FOR_JUDGMENT_AUTHOR;
    }
}