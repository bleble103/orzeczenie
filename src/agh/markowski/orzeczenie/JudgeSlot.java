package agh.markowski.orzeczenie;

import java.util.LinkedList;
import java.util.List;

public class JudgeSlot {
    private List<Roles> roles;
    private Judge judge;

    public JudgeSlot(Judge judge, List<Roles> roles){
        this.judge = judge;
        this.roles = roles;
    }

    public String getName(){
        return judge.getName();
    }

    public boolean hasRoles(){
        return this.roles.size() > 0;
    }

    public String getRolesString(){
        List<String> allRoles = new LinkedList<>();
        for(Roles role : roles){
            allRoles.add(role.toString());
        }
        return String.join(", ", allRoles);
    }
}
