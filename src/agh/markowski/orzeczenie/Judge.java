package agh.markowski.orzeczenie;

public class Judge {
    private int cases;
    private String name;

    public Judge(String name){
        this.name = name;
        this.cases = 1;
    }

    public void incrementCases(){
        this.cases ++;
    }

    public String getName(){
        return this.name;
    }

    public String getCasesString(){
        return String.format("\t%-20s %20s",this.name,"has "+this.cases+" cases");
    }

    public int getCases(){
        return this.cases;
    }

    @Override
    public String toString(){
        return this.name;
    }
}
