package agh.markowski.orzeczenie;

public enum CourtTypes {
    COMMON,
    SUPREME,
    ADMINISTRATIVE,
    CONSTITUTIONAL_TRIBUNAL,
    NATIONAL_APPEAL_CHAMBER;

    @Override
    public String toString(){
        switch(this){
            case COMMON:
                return "Sąd Powszechny";
            case SUPREME:
                return "Sąd Najwyższy";
            case ADMINISTRATIVE:
                return "Sąd Administracyjny";
            case CONSTITUTIONAL_TRIBUNAL:
                return "Trybunał Konstytucyjny";
            case NATIONAL_APPEAL_CHAMBER:
            default:
                return "Krajowa Izba Odwoławcza";
        }
    }

    public static CourtTypes toEnum(String courtType){
        if(courtType.equals("COMMON"))
            return COMMON;
        if(courtType.equals("SUPREME"))
            return SUPREME;
        if(courtType.equals("ADMINISTRATIVE"))
            return ADMINISTRATIVE;
        if(courtType.equals("CONSTITUTIONAL_TRIBUNAL"))
            return CONSTITUTIONAL_TRIBUNAL;
        else
            return NATIONAL_APPEAL_CHAMBER;
    }
}
