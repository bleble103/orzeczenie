package agh.markowski.orzeczenie.statistics;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class QuotaStatistics {
    private Map<Integer,Integer> quotas;

    public QuotaStatistics(){
        this.quotas = new HashMap<>();
    }

    public void reportQuota(int quota){
        if(quotas.containsKey(quota)){
            //increment immutable Integer
            Integer current = quotas.get(quota);
            quotas.replace(quota, current+1);
        }else{
            quotas.put(quota,new Integer(1));
        }
    }

    public String getStatistics(){
        String result = "\n";
        result += "Judges per case statistics:\n";

        int max = Collections.max(quotas.keySet());
        for(int i=0; i<= max; i++){
            //no such cases
            if(!quotas.containsKey(i)) continue;

            String partResult ="judge";
            if(i!=1) partResult += 's';
            partResult += " per case";
            result += String.format("\n\t%2d %-20s %5d",i,partResult,quotas.get(i));
        }
        result += '\n';
        return result;
    }
}
