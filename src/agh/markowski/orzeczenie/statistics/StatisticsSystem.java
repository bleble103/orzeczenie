package agh.markowski.orzeczenie.statistics;

public class StatisticsSystem {
    public final TimeStatistics timeStatistics;
    public final LawStatistics lawStatistics;
    public final CourtStatistics courtStatistics;
    public final QuotaStatistics quotaStatistics;

    public StatisticsSystem(){
        this.timeStatistics = new TimeStatistics();
        this.lawStatistics = new LawStatistics();
        this.courtStatistics = new CourtStatistics();
        this.quotaStatistics = new QuotaStatistics();
    }
}
