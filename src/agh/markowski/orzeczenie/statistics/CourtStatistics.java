package agh.markowski.orzeczenie.statistics;

import agh.markowski.orzeczenie.CourtTypes;

import java.util.*;

public class CourtStatistics {
    private Map<CourtTypes, Integer> courtTypes;
    private int allCases = 0;

    public CourtStatistics(){
        this.courtTypes = new EnumMap<>(CourtTypes.class);
    }

    public void reportCourtType(CourtTypes courtType){
        if(this.courtTypes.containsKey(courtType)){
            //Increment immutable Integer
            Integer current = this.courtTypes.get(courtType);
            this.courtTypes.replace(courtType, current+1);
        }else{
            this.courtTypes.put(courtType,new Integer(1));
        }
        this.allCases++;
    }

    public String getStatistics(){
        String result = "\n";
        result += "Court Type statistics:\n";

        Iterator<CourtTypes> iterator = courtTypes.keySet().iterator();
        while(iterator.hasNext()){
            CourtTypes type = iterator.next();
            double percentage = 100.0*courtTypes.get(type)/allCases;
            result += String.format("%-30s %6.2f %%\n",type,percentage);
        }
        return result;
    }

}
