package agh.markowski.orzeczenie.statistics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class TimeStatistics {
    private Map<String, Year> years;
    private int allCases = 0;

    public TimeStatistics(){
        this.years = new HashMap<>();
    }

    public void reportDate(String date){
        String[] dateParts = date.split("-");
        String year = dateParts[0];
        int month = Integer.parseInt(dateParts[1]);

        if(this.years.containsKey(year)){
            this.years.get(year).reportCase(month);
        }
        else{
            Year newYear = new Year(year);
            newYear.reportCase(month);
            this.years.put(year, newYear);
        }

        this.allCases++;
    }

    public String getStatistics(){
        String result = "";

        //sort years
        Year[] allYears = this.years.values().toArray(new Year[0]);
        //lambda comparator
        Arrays.sort(allYears,Comparator.comparingInt(Year::getYearInt));


        //list statistics for each year
        for(Year year : allYears){
            double percentage = 100.0*year.getAllCases()/allCases;
            result += String.format("\n%-20s \t%6.2f %% [*]\n",year.getYear()+":",percentage);
            result += year.monthlyStatistics() + '\n';
        }
        return result+'\n';
    }
}
