package agh.markowski.orzeczenie.statistics;


public class Law {
    private String content;
    private int quotes;

    public Law(String content){
        this.content = content;
        this.quotes = 1;
    }

    public boolean hasContent(String content){
        return this.content.equals(content);
    }
    public void quote(){
        this.quotes++;
    }

    public int getQuotes(){
        return this.quotes;
    }

    public String getQuotesString(){
        return String.format("\t%-5s - " + this.content, this.quotes);
    }

}
