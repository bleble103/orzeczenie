package agh.markowski.orzeczenie.statistics;

public class Year {
    private String year;
    private Integer[] cases;
    private int allCases = 0;

    public static String monthName(int month){
        switch(month){
            case 1:
                return "Styczeń";
            case 2:
                return "Luty";
            case 3:
                return "Marzec";
            case 4:
                return "Kwiecień";
            case 5:
                return "Maj";
            case 6:
                return "Czerwiec";
            case 7:
                return "Lipiec";
            case 8:
                return "Sierpień";
            case 9:
                return "Wrzesień";
            case 10:
                return "Październik";
            case 11:
                return "Listopad";
            case 12:
            default:
                return "Grudzień";
        }
    }

    public Year(String year){
        this.year = year;
        this.cases = new Integer[12 + 1];
        for(int i=0; i<12+1; i++){
            this.cases[i] = new Integer(0);
        }


    }

    public void reportCase(int month){
        this.cases[month]++;
        this.allCases++;
    }

    public int getYearInt(){return Integer.parseInt(this.year);}
    public String getYear(){
        return this.year;
    }

    public String monthlyStatistics(){
        String result = "";
        for(int month =1; month <= 12; month++){
            if(month > 1) result+='\n';
            double percentage = 100.0*cases[month]/allCases;
            result += String.format("\t%-20s %6.2f %%",monthName(month)+": ",percentage);
        }
        return result;
    }

    public int getAllCases(){
        return allCases;
    }
}
