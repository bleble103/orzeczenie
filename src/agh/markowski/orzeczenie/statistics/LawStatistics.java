package agh.markowski.orzeczenie.statistics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class LawStatistics {
    private Map<Integer, Law> laws;
    private Law[] topLaws;

    public LawStatistics(){
        this.laws = new HashMap<>();
    }

    public void quoteHTMLLaw(int year, int entry, String content){
        Integer key = 10000*year + entry;
        if(laws.containsKey(key)){
            laws.get(key).quote();
        }else{
            //Search by content
            Law[] allLaws = laws.values().toArray(new Law[0]);
            for(Law l : allLaws){
                if(l.hasContent(content)){
                    l.quote();
                    return;
                }
            }

            //New regulation
            laws.put(key, new Law(content));
        }
    }
    public void quoteLaw(int year, int entry, String title){
        Integer key = 10000*year + entry;
        if(laws.containsKey(key)){
            laws.get(key).quote();
        }
        else{
            laws.put(key,new Law(title));
        }
    }


    public void calculateTopRegulations(){
        int howMany = Integer.min(laws.size(), 10);
        System.out.println("Calculating top regulations...");
        this.topLaws = new Law[howMany];
        Law[] allLaws = this.laws.values().toArray(new Law[0]);
        //reversed lambda comparator
        Arrays.sort(allLaws,Comparator.comparingInt(Law::getQuotes).reversed());
        for(int i=0; i<howMany; i++) topLaws[i]=allLaws[i];
        System.out.println("Done");

    }

    public String getTopRegulations(){
        if(this.topLaws.length == 0)
            return "No Regulations!";

        String result = this.topLaws[0].getQuotesString();
        for(int i=1; i<topLaws.length; i++){
            result += '\n'+this.topLaws[i].getQuotesString();
        }
        return result;
    }
}
