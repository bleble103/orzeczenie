package agh.markowski.orzeczenie;

import java.util.*;

public class BigContainer {
    private Map<String, Judgment> judgments;
    private Map<String, Judge> judges;
    private Judge[] topJudges;

    public BigContainer(){
        this.judgments = new LinkedHashMap<>();
        this.judges = new HashMap<>();
    }

    public void addJudgement(Judgment judgment){
        this.judgments.put(judgment.getCaseNumber(), judgment);
    }

    public Judge reportJudge(String name){
        Judge thatJudge;
        if (judges.containsKey(name)){
            thatJudge = judges.get(name);
            thatJudge.incrementCases();
            return thatJudge;
        }
        thatJudge = new Judge(name);
        this.judges.put(name, thatJudge);
        return thatJudge;
    }

    public int judgmentsLength(){
        return this.judgments.size();
    }
    public int judgesLength(){
        return this.judges.size();
    }

    public String reasonFor(String caseNumber){
        return judgments.get(caseNumber).getReason();
    }

    public String infoAbout(String whichCaseNumber){
        Judgment currentJudgment = judgments.get(whichCaseNumber);
        return currentJudgment.infoAbout();

    }

    public String casesOf(String judgeName){
        Judge currentJudge = judges.get(judgeName);
        return currentJudge.getCasesString();
    }

    public boolean hasJudgment(String whichCaseNumber){
        return judgments.containsKey(whichCaseNumber);
    }


    public boolean hasJudge(String name){
        return judges.containsKey(name);
    }
    public Judge getJudge(String name){
        return judges.get(name);
    }

    public void calculateTopJudges(){
        int howMany = Integer.min(judges.size(), 10);
        System.out.println("Calculating top judges...");
        this.topJudges = new Judge[howMany];
        Judge[] allJudges = this.judges.values().toArray(new Judge[0]);

        //lambda reversed sort
        Arrays.sort(allJudges, Comparator.comparingInt(Judge::getCases).reversed());
        for(int i=0; i<howMany; i++) topJudges[i]=allJudges[i];
        System.out.println("Done");

    }

    public String getTopJudges(){
        if(this.topJudges.length == 0)
            return "No Judges!";

        String result = this.topJudges[0].getCasesString();
        for(int i=1; i<topJudges.length; i++){
            result += '\n'+this.topJudges[i].getCasesString();
        }
        return result;
    }


}
