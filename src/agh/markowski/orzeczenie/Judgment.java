package agh.markowski.orzeczenie;

import agh.markowski.orzeczenie.statistics.StatisticsSystem;
import agh.markowski.orzeczenie.statistics.Year;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class Judgment {
    private String caseNumber;
    private CourtTypes courtType;
    private JudgmentTypes judgmentType;
    private String date;
    private String reason;
    private List<JudgeSlot> judgeSlots;

    public Judgment(String caseNumber, CourtTypes courtType, JudgmentTypes judgmentType, String date, String reason, List<JudgeSlot> judgeSlots){
        //HTML constructor
        this.caseNumber = caseNumber;
        this.courtType = courtType;
        this.judgmentType = judgmentType;
        this.date = date;
        this.reason = reason;
        this.judgeSlots = judgeSlots;
    }

    public Judgment(JSONObject judgment, String caseNumber, List<JudgeSlot> judgeSlots, StatisticsSystem statisticsSystem){
        //JSON constructor
        this.caseNumber = caseNumber.toUpperCase();
        this.courtType = CourtTypes.toEnum(judgment.getString("courtType"));
        this.judgmentType = JudgmentTypes.toEnum(judgment.getString("judgmentType"));
        this.judgeSlots = judgeSlots;
        this.date = judgment.getString("judgmentDate");
        this.reason = judgment.getString("textContent");

        //court type statisticsSystem
        statisticsSystem.courtStatistics.reportCourtType(this.courtType);

        //time statisticsSystem
        statisticsSystem.timeStatistics.reportDate(this.date);

        //law Quote StatisticsSystem
        JSONArray referencedRegulations = judgment.getJSONArray("referencedRegulations");
        for(int i=0; i<referencedRegulations.length(); i++){
            JSONObject regulation = referencedRegulations.getJSONObject(i);
            int year = regulation.getInt("journalYear");
            int entry = regulation.getInt("journalEntry");
            String title = regulation.getString("journalTitle");
            statisticsSystem.lawStatistics.quoteLaw(year,entry,title);
        }

        //judge per case statistics
        statisticsSystem.quotaStatistics.reportQuota(judgeSlots.size());
    }

    public String getCaseNumber(){
        return this.caseNumber;
    }

    public CourtTypes getCourtType(){
        return this.courtType;
    }
    public JudgmentTypes getJudgmentType(){
        return this.judgmentType;
    }

    public String getReason(){
        String[] partReason = this.reason.split("<h2>UZASADNIENIE</h2>");
        if(partReason.length > 1){
            //html reason
            return partReason[1].replaceAll("<[^>]*>", "");
        }

        partReason = this.reason.split("UZASADNIENIE");
        if(partReason.length>1){
            return partReason[1];
        }

        //other format reason
        return this.reason;
    }

    public String infoAbout(){
        String result = "\nJudgment number: "+this.caseNumber+'\n';

        //Date translation
        String[] datePart = this.date.split("-");
        result += "Judgment date: "+datePart[2]+" "+ Year.monthName(Integer.parseInt(datePart[1]))+" "+datePart[0]+'\n';

        result += "Court type: "+this.courtType.toString()+'\n';
        result += "Judgment type: "+this.judgmentType.toString()+'\n';

        //List all judges
        String sJudges = "";
        for(int i=0; i<this.judgeSlots.size(); i++){
            JudgeSlot currentSlot = this.judgeSlots.get(i);

            //text formatting
            sJudges += "\n\t";

            sJudges += " "+this.judgeSlots.get(i).getName();
            if(currentSlot.hasRoles()){
                //this judge has some roles
                String roles = currentSlot.getRolesString();
                sJudges += " - "+roles;
            }
        }
        result += "Judges:" + sJudges + '\n';

        return result;
    }
}
