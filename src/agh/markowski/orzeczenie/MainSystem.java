package agh.markowski.orzeczenie;

import agh.markowski.orzeczenie.statistics.StatisticsSystem;
import jline.TerminalFactory;
import jline.UnsupportedTerminal;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

public class MainSystem {
    private static String fileExtension(File file){
        String[] f = file.getName().split("\\.");
        if(f.length == 0) return "";
        return f[f.length-1];
    }
    private static void listf(String directoryName, List<File> files) {
        //recursively gets all files and places them in list

        File directory = new File(directoryName);

        // Get all files from a directory.
        File[] fList = directory.listFiles();
        if(fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    files.add(file);
                } else if (file.isDirectory()) {
                    listf(file.getAbsolutePath(), files);
                }
            }
    }

    public static void main(String[] args){
        //windows console fix
        jline.TerminalFactory.registerFlavor(TerminalFactory.Flavor.WINDOWS, UnsupportedTerminal.class);

        BigContainer bigContainer = null;
        StatisticsSystem statisticsSystem = null;

        File outputFile = null;

        //get path from arg
        if(args.length > 0) {
            String path = args[0];
            List<File> allFiles = new LinkedList();
            listf(path, allFiles);

            //No files
            if(allFiles.size() == 0){
                System.out.println("No files to read!");
                System.exit(-2);
            }

            bigContainer = new BigContainer();
            statisticsSystem = new StatisticsSystem();
            FileParser fileParser = new FileParser(bigContainer, statisticsSystem);

            for(File f : allFiles) {
                System.out.println("Reading from " + f.getPath() + "...");
                if(fileExtension(f).equals("json")) {
                    //json file
                    fileParser.parseJSONFile(f);
                }
                else{
                    //html file
                    fileParser.parseHTMLFile(f);

                }
                System.out.println("Done");
            }
            if(args.length > 1){
                //write to file
                outputFile = new File(args[1]);
                try{
                    Files.write(outputFile.toPath(),"".getBytes());
                }catch(IOException e){
                    System.out.println("Can't create output file! "+e.getMessage());
                }
            }

            bigContainer.calculateTopJudges();
            statisticsSystem.lawStatistics.calculateTopRegulations();
        }
        else{
            System.out.println("No path specified!");
            System.exit(-1);
        }

        MyConsole console = new MyConsole(bigContainer, statisticsSystem, outputFile);
        console.start();
    }
}
