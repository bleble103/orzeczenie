package agh.markowski.orzeczenie;


import agh.markowski.orzeczenie.statistics.StatisticsSystem;
import jline.console.ConsoleReader;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class MyConsole {

    private BigContainer bigContainer;
    private StatisticsSystem statisticsSystem;
    private ConsoleReader console;
    private boolean writingToFile = false;
    private File outputFile = null;

    private void showHelp() throws IOException{
        output("Orzeczenie - zamiana tekstu na formę obiektową\n" +
                "Lista dostępnych komend:\n" +
                "\t rubrum <sygnatura> [, <sygnatura>, (...)] - wyświetla rubrum dla sprawy o określonej sygnaturze\n" +
                "\t content <sygnatura> [, <sygnatura>, (...)] - wyświetla uzasadnienie dla sprawy o określonej sygnaturze\n" +
                "\t judge <Imie Nazwisko> [, <Imie Nazwisko>, (...)] - wyświetla liczbę orzeczeń dla wybranego sędziego\n" +
                "\t judges - wyświetla 10 sędziów, którzy wydali najwięcej orzeczeń\n" +
                "\t months - wyświetla rozkład orzeczeń ze względu na miesiąc\n" +
                "\t courts - wyświetla rozkład orzeczeń ze względu na typ sądu\n" +
                "\t regulations - wyświetla 10 najczęściej przywoływanych ustaw\n" +
                "\t jury - wyświetla liczbę spraw przypadających na określoną liczbę sędziów\n" +
                "\t quit - wyjście z programu\n");
    }

    private void writeToFile(String line) throws IOException{
        Files.write(outputFile.toPath(),Arrays.asList(line), Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    }

    private void output(String line) throws IOException{
        console.println(line);
        if(writingToFile){
            writeToFile(line);
        }
    }

    private void giveQuotaStats() throws IOException{
        output(statisticsSystem.quotaStatistics.getStatistics());
    }

    private void giveCourtStats() throws IOException{
        output(statisticsSystem.courtStatistics.getStatistics());
    }

    private void giveTimeStats() throws IOException{
        output(statisticsSystem.timeStatistics.getStatistics());
    }

    private void giveTopRegulations() throws IOException{
        output("\nTop regulations:\n"+ statisticsSystem.lawStatistics.getTopRegulations()+ '\n');
    }
    private void giveTopJudges() throws IOException{
        output("\nTop judges:\n"+bigContainer.getTopJudges() + '\n');
    }

    private void giveJudgmentsLength() throws IOException{
        output("Number of judgments: "+bigContainer.judgmentsLength());
        //console.println("Debug: "+bigContainer.debug);
    }

    private void giveJudgesLength() throws IOException{
        output("Number of judges: "+bigContainer.judgesLength());
    }

    private void giveReasonFor(String caseNumber) throws IOException{
        if(bigContainer.hasJudgment(caseNumber)) {
            output(bigContainer.reasonFor(caseNumber));
        }
        else{
            output("Judgment "+caseNumber+" not found!");
        }
    }

    private void giveJudgmentInfo(String whichCaseNumber) throws IOException{
        if(bigContainer.hasJudgment(whichCaseNumber)) {
            output(bigContainer.infoAbout(whichCaseNumber));
        }
        else{
            output("Judgment "+whichCaseNumber+" not found!");
        }

    }

    private void giveJudgeInfo(String judgeName) throws IOException{
        if(bigContainer.hasJudge(judgeName)){
            output(bigContainer.casesOf(judgeName));
        }
        else{
            output("Judge "+judgeName+" not found!");
        }
    }


    public MyConsole(BigContainer bigContainer, StatisticsSystem statisticsSystem, File outputFile){
        this.bigContainer = bigContainer;
        this.statisticsSystem = statisticsSystem;
        if(outputFile != null){
            this.writingToFile = true;
            this.outputFile = outputFile;
        }
    }


    public void start() {
        try{
            console = new ConsoleReader();
            console.setPrompt("prompt> ");
            String line = null;
            while((line = console.readLine()) != null){
                //console.println(line);
                if(writingToFile)
                    writeToFile(">"+line);

                //close console
                if(line.equals("quit"))
                    break;

                //help
                if(line.equals("help")){
                    showHelp();
                    continue;
                }

                //Number of loaded judgments
                if(line.equals("judgments")){
                    giveJudgmentsLength();
                    continue;
                }

                //Number of loaded judges
                if(line.equals("judge_no")){
                    giveJudgesLength();
                    continue;
                }

                //Top 10 judges
                if(line.equals("judges")){
                    giveTopJudges();
                    continue;
                }

                //Top 10 regulations
                if(line.equals("regulations")){
                    giveTopRegulations();
                    continue;
                }

                //Time statistics
                if(line.equals("months")){
                    giveTimeStats();
                    continue;
                }

                //Court statistics
                if(line.equals("courts")){
                    giveCourtStats();
                    continue;
                }

                //Quota statistics
                if(line.equals("jury")){
                    giveQuotaStats();
                    continue;
                }

                // multi argument commands
                String[] command = line.split(" +");
                if(command.length >= 2){
                    String restOfCommand = String.join(" ",Arrays.copyOfRange(command,1,command.length));
                    String[] arguments = restOfCommand.split(",");
                    for(String argument : arguments){

                        //Reason for given case
                        if(command[0].equals("content"))
                            giveReasonFor(argument.trim().toUpperCase());

                        //Number of specific judge's cases
                        else if (command[0].equals("judge"))
                            giveJudgeInfo(argument.trim());

                        //Give info about specific judgment
                        else if(command[0].equals("rubrum"))
                            giveJudgmentInfo(argument.trim().toUpperCase());
                    }
                    continue;
                }
                console.println("Syntax error! Type help to see a list of commands...");

            }
            //End of console

        }
        catch (IOException e){
            System.out.println("Can't create a console: "+e.getMessage());
            System.exit(3);
        }
    }

}
