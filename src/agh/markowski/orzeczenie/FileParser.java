package agh.markowski.orzeczenie;

import agh.markowski.orzeczenie.statistics.StatisticsSystem;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

public class FileParser {
    private BigContainer bigContainer;
    private StatisticsSystem statisticsSystem;

    public FileParser(BigContainer bigContainer, StatisticsSystem statisticsSystem){
        this.bigContainer = bigContainer;
        this.statisticsSystem = statisticsSystem;
    }
    public void parseHTMLFile(File file){
        try {
            //caseNumber, judgmentType, date
            Document document = Jsoup.parse(new String(Files.readAllBytes(file.toPath())));
            String title = document.title();
            String[] splitTitle = title.split("-");
            String caseNumber = splitTitle[0].trim().toUpperCase();
            String[] splitYear = splitTitle[splitTitle.length-3].split(" ");
            String date = splitYear[splitYear.length-1]+'-'+splitTitle[splitTitle.length-2]+'-'+splitTitle[splitTitle.length-1];
            String judgmentTypeString = splitTitle[1].split(" ")[1];
            JudgmentTypes judgmentType = JudgmentTypes.fromHTML(judgmentTypeString);

            //judgeSlots
            Elements infoValues = document.getElementsByClass("info-list-value");
            String[] judgeEntries = infoValues.get(3).html().split("<br>");
            List<JudgeSlot> judgeSlots = new LinkedList<>();
            for(String judgeEntry : judgeEntries){
                String[] judgeSplit = judgeEntry.split("/");
                String judgeName = judgeSplit[0].trim();
                String judgeRoles = "";
                if(judgeSplit.length > 1)
                    judgeRoles = judgeSplit[1];

                Judge judge = bigContainer.reportJudge(judgeName);
                List<Roles> rolesList = new LinkedList<>();
                if(judgeRoles.contains("przewodnicz")){
                    rolesList.add(Roles.PRESIDING_JUDGE);
                }
                if(judgeRoles.contains("sprawozda")){
                    rolesList.add(Roles.REPORTING_JUDGE);
                }
                if(judgeRoles.contains("uzasadnie")){
                    rolesList.add(Roles.REASONS_FOR_JUDGMENT_AUTHOR);
                }

                judgeSlots.add(new JudgeSlot(judge,rolesList));
            }

            //reason
            String reason="";
            Elements candidates = document.getElementsByClass("info-list-label-uzasadnienie");
            for(int i=0; i<candidates.size(); i++){
                String content = candidates.get(i).getElementsByClass("lista-label").get(0).text();
                if(content.contains("Uzasadnienie")){
                    reason = candidates.get(i).getElementsByClass("info-list-value-uzasadnienie").get(0).text();
                    break;
                }
            }

            //add judgment
            this.bigContainer.addJudgement(new Judgment(caseNumber,CourtTypes.ADMINISTRATIVE, judgmentType, date, reason, judgeSlots));




            //statistics
            //Regulations statistics
            Elements labels = document.getElementsByClass("lista-label");
            int which = -1;
            for(int i=0; i<labels.size(); i++){
                if(labels.get(i).text().contains("przepisy")){
                    which = i;
                    break;
                }
            }
            if(which > -1){
                //regulations found
                Element value = document.getElementsByClass("info-list-value").get(which);
                Elements links = value.getElementsByTag("a");
                Elements spans = value.getElementsByClass("nakt");
                for(int i=0; i< links.size(); i++){
                    int year = Integer.parseInt(links.get(i).text().split(" ")[1]);
                    int entry = Integer.parseInt(links.get(i).text().split(" ")[3]);
                    String regulationContent = spans.get(i).text();
                    this.statisticsSystem.lawStatistics.quoteHTMLLaw(year,entry,regulationContent);
                }
            }

            //court type statistics
            this.statisticsSystem.courtStatistics.reportCourtType(CourtTypes.ADMINISTRATIVE);

            //judge quota statistics
            this.statisticsSystem.quotaStatistics.reportQuota(judgeSlots.size());

            //date statistics
            this.statisticsSystem.timeStatistics.reportDate(date);

        } catch (IOException e) {
            System.out.println("Problem while reading HTML file! "+e.getMessage());
        }
    }

    public void parseJSONFile(File file){
        try {

            JSONObject jsonObject = new JSONObject(new String(Files.readAllBytes(file.toPath())));
            JSONArray jsonArrayItems = jsonObject.getJSONArray("items");
            for (int i = 0; i < jsonArrayItems.length(); i++) {
                JSONObject jsonJudgment = jsonArrayItems.getJSONObject(i);
                //Judgment
                JSONArray cases = jsonJudgment.getJSONArray("courtCases");
                for(int j=0; j<cases.length(); j++){

                    List<JudgeSlot> judgeSlots = new LinkedList<>();
                    //Judges
                    JSONArray judges = jsonJudgment.getJSONArray("judges");
                    for(int k=0; k<judges.length(); k++){
                        Judge thatJudge = bigContainer.reportJudge(judges.getJSONObject(k).getString("name"));

                        //roles
                        List<Roles> rolesList = new LinkedList<>();
                        JSONArray roles = judges.getJSONObject(k).getJSONArray("specialRoles");
                        for(int l=0; l<roles.length(); l++){
                            rolesList.add(Roles.toEnum(roles.getString(l)));
                        }
                        //add slot to list
                        judgeSlots.add( new JudgeSlot(thatJudge, rolesList));
                    }

                    this.bigContainer.addJudgement(new Judgment(jsonJudgment, cases.getJSONObject(j).getString("caseNumber"), judgeSlots, statisticsSystem));

                }


            }
        }
        catch (JSONException e){
            System.out.println("JSON file: "+file.getPath()+" is invalid! "+e.getMessage());
            System.exit(5);
        }
        catch(IOException e){
            System.out.println("Can't find JSON file! "+e.getMessage());
        }
    }
}
