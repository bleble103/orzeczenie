package agh.markowski.orzeczenie;

public enum JudgmentTypes {
    DECISION,
    RESOLUTION,
    SENTENCE,
    REGULATION,
    REASONS;

    @Override
    public String toString(){
        switch(this){
            case DECISION:
                return "Postanowienie";
            case RESOLUTION:
                return "Uchwała";
            case SENTENCE:
                return "Wyrok";
            case REGULATION:
                return "Zarządzenie";
            case REASONS:
            default:
                return "Uzasadnienie";
        }
    }

    public static JudgmentTypes toEnum(String judgmentType){
        if(judgmentType.equals("DECISION"))
            return DECISION;
        if(judgmentType.equals("RESOLUTION"))
            return RESOLUTION;
        if(judgmentType.equals("SENTENCE"))
            return SENTENCE;
        if(judgmentType.equals("REGULATION"))
            return REGULATION;
        else
            return REASONS;
    }

    public static JudgmentTypes fromHTML(String judgmentType){
        if(judgmentType.contains("Wyrok")){
            return SENTENCE;
        }
        if(judgmentType.contains("Postanowienie")){
            return DECISION;
        }
        if(judgmentType.contains("Uchwa")){
            return RESOLUTION;
        }
        if(judgmentType.contains("Uzasadnienie")){
            return REASONS;
        }
        return REGULATION;
    }
}
